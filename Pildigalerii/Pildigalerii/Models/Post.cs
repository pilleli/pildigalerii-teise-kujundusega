//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pildigalerii.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Post
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Post()
        {
            this.PostInTags = new HashSet<PostInTag>();
            this.Comments = new HashSet<Comment>();
        }
    

        public int Id { get; set; }
        public Nullable<int> CategoryId { get; set; }
        [Display(Name = "Pildi nimi")]
        public string Title { get; set; }
        [Display(Name = "Kirjeldus")]
        public string Description { get; set; }
        [Display(Name = "Album")]
        public int AlbumId { get; set; }
        [Display(Name = "Omanik")]
        public int OwnerId { get; set; }
        [Display(Name = "Fail")]
        public int FileId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Kuup�ev")]
        public System.DateTime Created { get; set; }
        [Display(Name = "N�htavus")]
        public string Access { get; set; }
        [Display(Name = "")]
        public Nullable<int> grade { get; set; }

        public virtual Album Album { get; set; }
        public virtual Category Category { get; set; }
        public virtual DataFile DataFile { get; set; }
        public virtual Person Person { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PostInTag> PostInTags { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

    }
}
