﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pildigalerii.Models;

namespace Pildigalerii.Models
{
    public partial class Comment
    {
        //siin täiendan  klassi Comment
        static TagaridaEntities db = new TagaridaEntities();

        public static IEnumerable<Comment> PostComments(int postId)             //objektifunktsioon postituse kommentaaride leidmiseks
            => db.Comments.Where(x => x.PostId == postId).OrderByDescending(item => item.CommentDate).ToList();


        public static int CountById(int postId)
            => db.Comments.Where(x => x.PostId == postId).Count();
    }
}


namespace Pildigalerii.Models
{
    public class CommentsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Comments
        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Person).Include(c => c.Post);
            return View(comments.ToList());
        }     

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            ViewBag.PersonID = new SelectList(db.People, "Id", "FirstName");
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");

            Comment comment = new Comment { CommentDate = DateTime.Today };
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PostId,CommentTxt,CommentDate")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                comment.CommentDate = DateTime.Now;
                comment.PersonID = 1;
                db.SaveChanges();
                return RedirectToAction("Details", "Posts", new { id = comment.PostId });
            }

            ViewBag.PersonID = new SelectList(db.People, "Id", "FirstName", comment.PersonID);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            //return RedirectToAction("Details", "Posts", new { });
            return View(comment);
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonID = new SelectList(db.People, "Id", "FirstName", comment.PersonID);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PostId,PersonID,CommentTxt,CommentDate")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonID = new SelectList(db.People, "Id", "FirstName", comment.PersonID);
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", comment.PostId);
            return View(comment);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
