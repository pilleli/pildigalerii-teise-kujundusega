﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pildigalerii.Models;

namespace Pildigalerii.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            TagaridaEntities db = new TagaridaEntities();
            //missugused albumid üldse on olemas
            ViewBag.Albumid = db.Albums.ToList();

            //4 kõige uuemat posti, mis on publicud, selleks on PostControlleris defineeritud GetNew(nr)
            ViewBag.NewImg = Post.GetNew(4);
            ViewBag.PopularImg = Post.GetPopular(8);
            return View();
        }


        public ActionResult MinuUser()
        {
            ViewBag.Message = "Minu profiil";
            TagaridaEntities db = new TagaridaEntities();
            //kes on praegu sisse loginud
            Person person = Person.GetByEmail(User.Identity.Name);
            ViewBag.CurrentUser = person;
            //missugused albumid on nüüd sellel sisseloginud kasutajaltry 
            try
            {

                ViewBag.MinuAlbumid = db.Albums.Where(y => y.OwnerId == person.Id).OrderByDescending(item => item.Created).ToList();
            }

    catch (Exception)
            {

                throw;
            }
            
            //kuidas leida, mitu pilti seal albumis on ja panna see sulgudesse albumi taha
            //Post post = db.Posts.Where(a => a.AlbumId == 5).SingleOrDefault();

            if (Request.IsAuthenticated)
                return View();
            else return RedirectToAction("Register", "Account");
        }
    }
}