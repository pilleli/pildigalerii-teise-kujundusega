﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pildigalerii.Models;

namespace Pildigalerii.Models
{
 
    public partial class Post
    {
        //siin täiendan  klassi Post
        static TagaridaEntities db = new TagaridaEntities();

        public static Post getElementById(int id)                       //meetod pildi(posti) leidmiseks Id järgi
            => db.Posts.Where(a => a.Id == id).SingleOrDefault();

        public static IEnumerable<Post> GetNew(int nr)             //objektifunktsioon kõige uuemate (nr tükki) avalike postituste leidmiseks 
            => db.Posts.Where(x => x.Album.Access == "public").OrderByDescending(item => item.Created).Take(nr).ToList();

        public static IEnumerable<Post> GetPopular(int nr)             //objektifunktsioon kõige popimate (nr tükki) avalike postituste leidmiseks 
            => db.Posts.Where(x => x.Album.Access == "public").OrderByDescending(item => item.grade).Take(nr).ToList();

    }


}

namespace Pildigalerii.Controllers
{
    public class PostsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Posts

        //Järgnev lõik on Hennu reedesest näitest, Anna katsetas. Vist vajalik faili uploadimise jaoks.
        public ActionResult GetPicture(int? id)
        {
            Post p = db.Posts.Find(id ?? 0);
            if (p == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (p.DataFile?.Content?.Length == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return File(p.DataFile.Content.ToArray(), p.DataFile.ContentType);
        }

        public ActionResult Index(string sortOrder)
        {
            //var posts = db.Posts.Where(x => x.Album.Access == "public").Include(p => p.Album).Include(p => p.Category).Include(p => p.DataFile).Include(p => p.Person).Include(p=>p.Comments);

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.OmanikSortParm = String.IsNullOrEmpty(sortOrder) ? "omanik" : "";
            ViewBag.AlbumSortParm = String.IsNullOrEmpty(sortOrder) ? "Album" : "";
            ViewBag.ComSortParm = sortOrder == "com_desc" ? "Comment" : "com_desc";
            ViewBag.LikeSortParm = sortOrder == "like_desc" ? "Like" : "like_desc";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var posts = from s in db.Posts.Where(x => x.Album.Access == "public").Include(p => p.Album).Include(p => p.Category).Include(p => p.DataFile).Include(p => p.Person).Include(p => p.Comments)
                        select s;
            switch (sortOrder)
            {
                case "name_desc":
                    posts = posts.OrderByDescending(s => s.Title);
                    break;
                case "omanik":
                    posts = posts.OrderBy(s => s.OwnerId);
                    break;
                case "like_desc":
                    posts = posts.OrderByDescending(s => s.grade);
                    break;
                case "com_desc":
                    posts = posts.OrderByDescending(s => s.Comments.Count);
                    break;
                case "Album":
                    posts = posts.OrderBy(s => s.Album.Title);
                    break;
                case "Comments":
                    posts = posts.OrderBy(s => s.Comments.Count);
                    break;
                case "Like":
                    posts = posts.OrderBy(s => s.grade);
                    break;
                case "Date":
                    posts = posts.OrderBy(s => s.Created);
                    break;
                case "date_desc":
                    posts = posts.OrderByDescending(s => s.Created);
                    break;
                default:
                    posts = posts.OrderByDescending(s => s.Created);
                    break;
            }
            return View(posts.ToList());
        }


        public ActionResult IndexAlbum(int albumId)
        {
            var posts = db.Posts
                .Where(x => x.AlbumId == albumId)
                .Include(p => p.Album).Include(p => p.Category).Include(p => p.DataFile).Include(p => p.Person);
            return View(posts.ToList());
        }



        // GET: Posts/Details/5
        public ActionResult Details(int? id, string dir="current")  // deafult väärtus on et näidatase selle id-ga pilti, kui dir panna next - siis näidatakse järgmist pilti, kui dir= prev siis eelmist pilti
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);

            // siin määratakse mis juhtub kui dir= next - järgmine pilt albumist
            if (dir == "next")
            {
                post = post.Album.Posts.OrderBy(x => x.Created).Where(x => x.Created > post.Created).FirstOrDefault() ?? post;
            }
            else if (dir == "prev")
            {
                post = post.Album.Posts.OrderByDescending(x => x.Created).Where(x => x.Created < post.Created).FirstOrDefault() ?? post;
            }
            //


            ViewBag.Grade = post.grade;
            if (post == null)
            {
                return HttpNotFound();
            }

            ViewBag.CommentCount = Comment.CountById(post.Id); // mitu kommentaari on?
            ViewBag.Comments = Comment.PostComments(post.Id); // et saaks Views kuvada kommentaare

            if (Request.IsAuthenticated)
            {
                try
                {
                    int personId = Person.CurrentUserID(User.Identity.Name);
                    ViewBag.PersonId = Person.CurrentUserID(User.Identity.Name);
                    ViewBag.UserName = db.People.Where(x => x.Id == personId);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            return View(post);
        }

        // GET: Posts/Create
        public ActionResult Create(int? albumId)
        {
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Title", albumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "FileName");
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName");
            
            int personId = Person.CurrentUserID(User.Identity.Name);
            ViewBag.UserName = db.People.Where(x => x.Id == personId);
            ViewBag.UserAlbums = db.Albums.Where(x => x.OwnerId == personId); // et saaks Views kuvada ainult sisse logitud kasutaja albumeid
            Post post = new Post { OwnerId = personId, Created = DateTime.Now, Access = "public"}; // ilma access publicuta ei lase creatida
            return View(post);
        }


        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryId,Title,Description,AlbumId,OwnerId,FileId,Created,Access,grade")] Post post, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile pic = new DataFile()

                        {
                            Content = br.ReadBytes(file.ContentLength),
                            FileName = file.FileName.Split('\\').LastOrDefault().Split('/').Last(),
                            ContentType = file.ContentType
                        };
                        db.SaveChanges();

                        db.Posts.Add(post);
                        post.Created = DateTime.Now;
                        post.grade = 0;
                        post.DataFile = pic;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Edit", "Albums", new { id = post.AlbumId });
            }
                ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Title", post.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", post.CategoryId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "FileName", post.FileId);
                ViewBag.OwnerName = new SelectList(db.People, "Name", "FirstName", post.OwnerId);
                return View(post);

        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if (post.OwnerId == Person.CurrentUserID(User.Identity.Name)) // kontrollib, et pildi omanik oleks current user, et ei saaks teiste pilte kustutada
            {
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Title", post.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", post.CategoryId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "FileName", post.FileId);
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName", post.OwnerId);
            int personId = Person.CurrentUserID(User.Identity.Name);
            ViewBag.UserAlbums = new SelectList(db.Albums.Where(x => x.OwnerId == personId), "Id", "Title", post.AlbumId); // et saaks Views kuvada ainult sisse logitud kasutaja albumeid
            return View(post);
            }
            else

                return RedirectToAction("Details");
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryId,Title,Description,AlbumId,OwnerId,Access,grade")] Post post, HttpPostedFileBase file)
        {

                if (ModelState.IsValid)
                {
                    db.Entry(post).State = EntityState.Modified;
                    db.Entry(post).Property("Created").IsModified = false; // seda oli vaja, et datetime ei viskaks errorit, kuna SQL ei suuda convertida datetime2 -> datetime
                    db.Entry(post).Property("FileId").IsModified = false; // see rida tagab selle, et kui muutmisvorm ilma uue pildita üle salvestada, ei läheks pilt kaotsi
                    db.Entry(post).Property("AlbumId").IsModified = false; // see rida tagab selle, et kui muutmisvorm ilma uue pildita üle salvestada, ei läheks pilt kaotsi
                    db.SaveChanges();
                    if (file != null && file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            DataFile pic = new DataFile()

                            {
                                Content = br.ReadBytes(file.ContentLength),
                                FileName = file.FileName.Split('\\').LastOrDefault().Split('/').Last(),
                                ContentType = file.ContentType
                            };
                            post.DataFile = pic;
                            db.SaveChanges();
                        }
                    }
                    return RedirectToAction("Index");

            }
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Title", post.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", post.CategoryId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "FileName", post.FileId);
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName", post.OwnerId);
            return View(post);

        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            if (post.OwnerId == Person.CurrentUserID(User.Identity.Name)) // kontrollib, et pildi omanik oleks current user, et ei saaks teiste pilte kustutada
            {

                return View(post);
            }
            else

                return RedirectToAction("Details");
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);


                try
            {
                    db.DataFiles.Remove(post.DataFile);  // kuipilt teistega seotud pole, siis kustuta ka see datafile ära
                }
                catch (Exception)
                {

                    throw;
                }
                DeleteComments(post);        //kustuta ära ka kommentaarid
                db.Posts.Remove(post);
                db.SaveChanges();
                return RedirectToAction("Index");

        }        
        // Kustutab ka need kommentaarid ära, mis selle posti küljes on

        public void DeleteComments(Post a)
        {
            if (a.Comments != null && a.Comments.Count > 0)
            {
                db.Comments.RemoveRange(a.Comments);
            }
        }

        // Like panemise funktsioon, mis otsib ID järgi pildi, lisab sellele like'i, salvestab andmebaasi ja viib tagasi Details lehele
        public ActionResult AddLike(int id)
        {
            Post post = db.Posts.Find(id);
            if (post != null)
            {
                post.grade++;
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id = id });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
