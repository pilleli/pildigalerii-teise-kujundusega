﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pildigalerii.Models;
using System.IO;

namespace Pildigalerii.Models
{
    public partial class Person
    {
        // siin vahel täiendada klassi Person
        static TagaridaEntities db = new TagaridaEntities();

        public static Person GetByEmail(string email)
            => db.People.Where(x => x.EMail == email).SingleOrDefault();
        
        public static int CurrentUserID(string email)
            => db.People.Where(x => x.EMail == email).SingleOrDefault().Id;

        public static implicit operator Person(int v)
        {
            throw new NotImplementedException();
        }
    }
}

namespace Pildigalerii.Controllers
{
    public class PeopleController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        //GET: People

        //Järgnev lõik on Hennu reedesest näitest, Anna katsetas. Võib vabalt välja kommenteerida.
        public ActionResult GetPicture(int? id)
        {
            Person p = db.People.Find(id ?? 0);
            if (p == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (p.DataFile?.Content?.Length == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return File(p.DataFile.Content.ToArray(),p.DataFile.ContentType);
        }

        public ActionResult Index(string sortOrder)
        {

                Person p = Person.GetByEmail(User.Identity.Name);
            if (p == null) return RedirectToAction("Create");

            ViewBag.Albumid = db.Albums.ToList();

            //var people = db.People.Include(x => x.DataFile);

            //Päiste sorteerimiseks
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var people = from s in db.People.Include(x => x.DataFile)
                         select s;
            switch (sortOrder)
            {
                case "name_desc":
                    people = people.OrderByDescending(s => s.FirstName);
                    break;
                case "Date":
                    people = people.OrderBy(s => s.BirthDate);
                    break;
                case "date_desc":
                    people = people.OrderByDescending(s => s.BirthDate);
                    break;
                default:
                    people = people.OrderBy(s => s.FirstName);
                    break;
            }

            return View(people.ToList());
        }


        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            // kasutaja albumid (ei tea, kas töötab)
            ViewBag.UserAlbums = person.Albums.ToList();
            RedirectToAction("Posts");

            return View(person);



        }

        

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "FileName");
            Person person = new Person { EMail = User.Identity.Name, Active = true };
            return View(person);
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,BirthDate,Active,EMail")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile pic = new DataFile()

                        {
                            Content = br.ReadBytes(file.ContentLength),
                            FileName = file.FileName.Split('\\').LastOrDefault().Split('/').Last(),
                            ContentType = file.ContentType
                        };
                        person.DataFile = pic;
                        db.SaveChanges();
                    }
                }
            return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "FileName", person.PictureId);
            return View(person);


        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "FileName", person.PictureId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,BirthDate,Active,PictureId,EMail")] Person person, HttpPostedFileBase file )
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property("PictureId").IsModified = false; // see rida tagab selle, et kui muutmisvorm ilma uue pildita üle salvestada, ei läheks pilt kaotsi
                db.SaveChanges();
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        DataFile pic = new DataFile()

                        {
                            Content = br.ReadBytes(file.ContentLength),
                            FileName = file.FileName.Split('\\').LastOrDefault().Split('/').Last(),
                            ContentType = file.ContentType
                        };
                        person.DataFile = pic;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "FileName", person.PictureId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]

        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        

    }
}
