﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pildigalerii.Models;



namespace Pildigalerii.Controllers
{


    public class AlbumsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();
        
        // GET: Albums
        //public ActionResult Index()

        //{
        //    var albums = db.Albums.Include(a => a.Person);
        //    if (Request.IsAuthenticated)
        //        return View(albums.ToList());
        //    else return RedirectToAction("IndexPublic");

        //}

        // GET: Public albums
        public ActionResult Index(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.OmanikSortParm = String.IsNullOrEmpty(sortOrder) ? "omanik_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var albums = from s in db.Albums.Where(x => x.Access == "public").Include(a => a.Person)
                        select s;
            switch (sortOrder)
            {
                case "name_desc":
                    albums = albums.OrderByDescending(s => s.Title);
                    break;
                case "omanik_desc":
                    albums = albums.OrderByDescending(s => s.OwnerId);
                    break;
                case "Date":
                    albums = albums.OrderBy(s => s.Created);
                    break;
                case "date_desc":
                    albums = albums.OrderByDescending(s => s.Created);
                    break;
                default:
                    albums = albums.OrderBy(s => s.Title);
                    break;
            }
            return View(albums.ToList());
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }

            //missugused pildid  on olemas
            //ViewBag.AlbumImgs = db.Posts.Where(y => y.AlbumId == album.Id).ToList();
            ViewBag.AlbumImgs = album.Posts.OrderByDescending(item => item.Created).ToList();


            return View(album);
        }

        // GET: Albums/Create
        public ActionResult Create()
        {
            ViewBag.OwnerId = new SelectList(db.People, "Id", "Fullname");

            //kes on praegu sisse loginud
           int personId = Person.CurrentUserID(User.Identity.Name);
            ViewBag.UserName = db.People.Where(x => x.Id == personId);

            Album album = new Album { OwnerId = personId, Created = DateTime.Today, Access = "public" }; // ilma access publicuta ei lase creatida

            return View(album);
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,OwnerId,Access,Created")] Album album)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.Albums.Add(album);
                        db.SaveChanges();
                        return RedirectToAction("MinuUser", "Home");
                    }

                    ViewBag.OwnerId = new SelectList(db.People, "Id", "FullName", album.OwnerId);
                    return View(album);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            else return RedirectToAction("Index");
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FullName", album.OwnerId);

            //missugused pildid  on olemas
            //ViewBag.AlbumImgs = db.Posts.Where(y => y.AlbumId == album.Id).ToList();
            ViewBag.AlbumImgs = album.Posts.OrderByDescending(item => item.Created).ToList();

            return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,OwnerId,Access")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.Entry(album).Property("Created").IsModified = false; // seda oli vaja, et datetime ei viskaks errorit, kuna SQL ei suuda convertida datetime2 -> datetime
                //db.Entry(album).Property("Access").IsModified = false; //seda ära tagasi sisse kommenteeri, muidu ei salvesta andmebaasi privaatsuse (access) muudatust, kui muuta.
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FullName", album.OwnerId);
            return View(album);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            DeletePosts(album);
            db.Albums.Remove(album);
            db.SaveChanges();
            if (Request.IsAuthenticated)
                return RedirectToAction("MinuUser", "Home");
            else
                return RedirectToAction("Index");
        }

        // Kustutab ka need postitused ära, mis selle albumi sees on

        private void DeletePosts(Album a)
        {
            if (a.Posts != null && a.Posts.Count > 0)
            {
                db.Posts.RemoveRange(a.Posts);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class Time
        {
            private DateTime _date = DateTime.Now;

            public int ID { get; set; }

            public DateTime DateCreated
            {
                get { return _date; }
                set { _date = value; }
            }

        }
    }
}
